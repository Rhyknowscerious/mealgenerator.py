'''Meal Generator

This application generates lists of random ingredients so you can have an 
idea about what you might want to make tonight for dinner.

To use this package in IDLE, do this:

import mealgenerator

mealgenerator.launch()
'''

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright 2019 Ryan James Decker

from mealgenerator import *
