# Meal Generator

This is a little application that gives you ideas for meal preparation.

This was written in Python2.7 and uses the Tkinter module for window design.

To use Meal Generator, go to the command line and type `python -m mealgenerator`.
